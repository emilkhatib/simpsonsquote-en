CREATE TABLE IF NOT EXISTS `simpson_quote` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `character` varchar(100) NOT NULL,
  `episode` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `simpson_quote` (`id`, `content`, `character`, `episode`, `season`) VALUES (NULL, 'I, for one, welcome our new insect overlords.', 'Kent Brockman', '15', '5');

INSERT INTO `simpson_quote` (`id`, `content`, `character`, `episode`, `season`) VALUES (NULL, 'You tried your best and you failed miserably. The lesson is: Never try.', 'Homer Simpson', '18', '5');

INSERT INTO `simpson_quote` (`id`, `content`, `character`, `episode`, `season`) VALUES (NULL, 'Don\'t blame me, I voted for Kodos.', 'Homer Simpson', '1', '8');

INSERT INTO `simpson_quote` (`id`, `content`, `character`, `episode`, `season`) VALUES (NULL, 'My cat\'s breath smells like cat food.', 'Ralph Wiggum', '2', '6');

INSERT INTO `simpson_quote` (`id`, `content`, `character`, `episode`, `season`) VALUES (NULL, 'Life is just one crushing defeat after another until you just wish Flanders was dead.', 'Homer Simpson', '13', '5');
