from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from SQapp.models import SimpsonQuote
from SQapp.serializers import QuoteSerializer


# This is the first example we used to introduce Django (http://www.emilkhatib.com/serving-simpsons-quotes-with-django/)
def randomquote(request):
	quote = SimpsonQuote.objects.order_by('?').first() # Randomize the order of the entries and pick the first one
	response = '\"' + quote.content + '\" ' + quote.character + ' (S' + str(quote.season) + 'E' + str(quote.episode) + ')' 
	return HttpResponse(response) # Return a simple text


# This second example uses the templates to render a response. (http://www.emilkhatib.com/using-django-templates/)
# Note that in order to use this, you must deactivate the user authentication system, by setting 'DEFAULT_PERMISSION_CLASSES' to 
# 'rest_framework.permissions.AllowAny' in settings.py

def quotesearch(request):
	if request.method == 'GET':
		# First user request returns a search form
		return render(request, 'searchform.html')
	else:
		# Second user request posts the search terms
		searchterms = request.POST['terms'].split()  # Separate the terms and save them in a list
		if len(searchterms) >= 1:
			searchresults = SimpsonQuote.objects.filter(content__icontains = searchterms[0]) # Filter the quotes by the content field that contains the first term
			for additional_term in searchterms[1:]: # Filter by any additional terms
				searchresults = searchresults.filter(content__icontains = additional_term)
			if len(searchresults) > 0:
				return render(request, 'searchresults.html', {'results':searchresults}) # Render and return the results
			else:
				return render(request, 'error.html', {'message':'No results found'}) # If nothing was found, notify the user
		else:
			# Do some error handling here if the client sends an empty request
			return render(request, 'error.html', {'message':'Please enter some search terms'})


# This example implements an API View for the REST Framework (http://www.emilkhatib.com/introducing-django-rest-framework/)
class QuoteSearch(APIView):
	def get(self, request):
		# First request, send the form. With the final version of the protocol (http://www.emilkhatib.com/authentication-with-django-rest-framework/) this remains unused
		return render(request, 'searchform.html')

	def post(self, request):
		# Second request. Works just like in the second example; but changing templates by serializers
		searchterms = request.POST['terms'].split()
		if len(searchterms) >= 1:
			searchresults = SimpsonQuote.objects.filter(content__icontains = searchterms[0])
			for additional_term in searchterms[1:]: 
				searchresults = searchresults.filter(content__icontains = additional_term)
			if len(searchresults) > 0:
				serializer = QuoteSerializer(searchresults, many=True)
				return Response(serializer.data)
			else:
				return Response({'message':'No results found'})
		else:
			return Response({'message':'Please enter some search terms'})
	
