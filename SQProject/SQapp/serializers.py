from rest_framework import serializers
from SQapp.models import SimpsonQuote

class QuoteSerializer(serializers.ModelSerializer):
	class Meta:
		model = SimpsonQuote

