from django.conf.urls import url
from django.contrib import admin
from SQapp.views import randomquote, quotesearch, QuoteSearch
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^randomquote/', randomquote),
    url(r'^search/', QuoteSearch.as_view()),
    url(r'^search-html/', quotesearch),
    url(r'^api-token-auth/', obtain_auth_token),
]
